window.onload = () => {
  const canvas = document.getElementById('canvas');
  const context = canvas.getContext('2d');
  const pixelRatio = window.devicePixelRatio;
  const canvasWidth = Math.floor(pixelRatio * 100);
  canvas.width = canvasWidth;
  canvas.height = canvasWidth;

  context.strokeStyle = 'hsl(0, 0%, 5%)';
  context.lineWidth = 1.2 * pixelRatio;

  const rnd = () => (Math.random() >= 0.5 ? Math.random() : -Math.random());
  const size = canvas.width / 4;
  const curve = canvas.width / 3;
  const centerX = canvas.width / 2;
  const centerY = canvas.height / 2;
  const N = 100;

  context.beginPath();
  context.moveTo(centerX, centerY);
  for (let i = 0; i < N; i++) {
    const x = centerX + size * rnd();
    const y = centerY + size * rnd();
    context.bezierCurveTo(
      x + curve * rnd(),
      y + curve * rnd(),
      x + curve * rnd(),
      y + curve * rnd(),
      x,
      y
    );
  }
  context.stroke();

  if (pixelRatio > 1) {
    canvas.style.width = '4.125rem';
    canvas.style.height = '4.125rem';
  } else {
    canvas.style.height = '3.125rem';
    canvas.style.width = '3.125rem';
  }
};
