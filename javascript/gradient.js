window.onload = () => {
  if (window.innerWidth > 800) {
    document.addEventListener('mousemove', event => {
      const cursor = document.getElementById('cursor');
      cursor.style.left = `${event.pageX - 125}px`;
      cursor.style.top = `${event.pageY - 125}px`;
    });
  }
};
